FROM golang:1.22 as builder

# Arbeitsverzeichnis im Container setzen
WORKDIR /app

# Go Kram kopieren
COPY go.mod ./
COPY main.go .

# Statische Kompilierung des Go-Programms
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o hallobootcamp .

# Zweite Stufe: Erstellen eines leeren Images
FROM scratch

# Kopieren des kompilierten Programms aus dem Builder-Image
COPY --from=builder /app/hallobootcamp .

CMD ["./hallobootcamp"]

